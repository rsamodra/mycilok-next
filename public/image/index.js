import bgImage from "./bg.jpg";
import cilokIcon from "./cilokIcon.png";
import scrollDown from "./scrolldown.svg";
import gameImage from "./game3.jpg";
import gameImage2 from "./game5.jpg";
import gameImage3 from "./game2.jpg";
import profileImage from "./profile.jpeg";
import rockImage from "./rock.png";
import paperImage from "./paper.png";
import scissorsImage from "./scissors.png";
import refreshImage from "./refresh.png";
import backIcon from "./back.png";
import RPSImage from "./rps-logo.png";

export {
  bgImage,
  cilokIcon,
  scrollDown,
  gameImage,
  gameImage2,
  gameImage3,
  profileImage,
  rockImage,
  paperImage,
  scissorsImage,
  refreshImage,
  backIcon,
  RPSImage,
};
