import Header from "../Header";
import Footer from "../Footer/Footer";

export default function Layout({ children }) {
  // const {children} = props
  return (
    <div>
      <Header />
      <main>{children}</main>
      <Footer />
    </div>
  );
}
