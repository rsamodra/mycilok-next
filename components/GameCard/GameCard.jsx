import React, { Component } from "react";
import Link from "next/link";
import { gameImage } from "../../public/image";
import Image from "next/image";

class GameCard extends Component {
  state = {};
  render() {
    const { img, title, detail, uuid } = this.props;
    const gameTitle = title === "RPS" ? "Play" : "Cooming soon";
    const gameClass =
      gameTitle === "Cooming soon"
        ? "btn btn-warning shadow-lg disabled"
        : "btn btn-warning shadow-lg";

    return (
      <div
        className="card mx-auto shadow-lg mt-3 bg-dark text-white px-0"
        style={{
          width: "23rem",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Image
          src={img ? img : gameImage}
          className="card-img-top"
          alt="..."
          width={380}
          height={250}
        ></Image>
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{detail}</p>
          {/* <p className="btn-danger">{uuid}</p> */}
          <div className="pl-0">
            <Link href={`/game/${uuid}`}>
              <a className="btn btn-info shadow-lg me-2">Game Detail</a>
            </Link>
            <Link href={`/game/play/${uuid}`}>
              <a className={gameClass}>{gameTitle}</a>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default GameCard;
