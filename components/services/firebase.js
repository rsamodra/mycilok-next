import firebase from "firebase";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB7B_Ga_BFArlcxHaGVrXiahmRerrpC54M",
  authDomain: "ilham-firebase-web.firebaseapp.com",
  projectId: "ilham-firebase-web",
  storageBucket: "ilham-firebase-web.appspot.com",
  messagingSenderId: "624111292645",
  appId: "1:624111292645:web:53186572aae83d1d1e040c",
};

export default firebase.default.initializeApp(firebaseConfig);
