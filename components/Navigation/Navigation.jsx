import React, { Component } from "react";
import styles from "./Navigation.module.css";
import { cilokIcon } from "../../public/image";
import { Container, Nav, Navbar } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";
// import firebase from "../services/firebase";
// import { AuthContext } from "../auth/auth";

class Navigation extends Component {
  //   static contextType = AuthContext;
  state = { isOpen: false };
  toggleCollapse = () => {};

  handleLogout = () => {
    const answer = window.confirm("Anda yakin ingin logout?");
    if (answer) {
      firebase.auth().signOut();
      alert("Anda telah logout!");
    } else {
      alert("Please enjoy our game!");
    }
  };

  render() {
    const { currentUser } = this.context;
    // console.log(currentUser);
    return (
      <Navbar
        bg="dark"
        variant="dark"
        expand="lg"
        sticky="top"
        // className="navbar py-0 shadow-lg bg-dark"
        className={styles.containerNav}
      >
        <Container>
          <Navbar.Brand href="/">
            <div>
              <Image
                src="/image/cilokIcon.png"
                className="logo"
                alt="MyCilok"
                width={75}
                height={75}
              />
              MyCilok
            </div>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav
              as="ul"
              className="ms-lg-5 justify-content-between"
              style={{ width: "15%" }}
            >
              {/* <Nav.Item as="li"> */}
              <li className={styles.item}>
                <Link href="/">
                  <a className="nav-link">Home</a>
                </Link>
                {/* </Nav.Item> */}
              </li>
              <li className={styles.item}>
                <Link href="/game">
                  <a className="nav-link">Game</a>
                </Link>
              </li>
            </Nav>
            {currentUser ? (
              <Nav
                as="ul"
                className="ms-lg-auto justify-content-between"
                style={{ width: "20%" }}
              >
                <li className={styles.item}>
                  <Link href="/profile">
                    <a className="nav-link">Profile</a>
                  </Link>
                </li>
                <li className={styles.item}>
                  <Link href="/" onClick={this.handleLogout}>
                    <a className="nav-link">Logout</a>
                  </Link>
                </li>
              </Nav>
            ) : (
              <Nav
                as="ul"
                className="ms-lg-auto justify-content-between"
                style={{ width: "20%" }}
              >
                <li className={styles.item}>
                  <Link href="/register">
                    <a className="nav-link">Register</a>
                  </Link>
                </li>
                <li className={styles.item}>
                  <Link href="/login">
                    <a className="nav-link">Login</a>
                  </Link>
                </li>
              </Nav>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default Navigation;
