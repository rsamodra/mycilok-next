import { Carousel } from "react-bootstrap";
import Image from "next/image";

export default function GameCarousel() {
  return (
    <div className="container">
      <Carousel variant="dark">
        <Carousel.Item>
          <Image
            className="d-block w-100 h-75"
            src="https://wallpapercave.com/wp/wp2936648.jpg"
            alt="First slide"
            width="50%"
            height="50%"
          />
          <Carousel.Caption>
            <h5>First slide label</h5>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image
            className="d-block w-100 h-75"
            src="https://wallpapercave.com/wp/wp2936651.jpg"
            alt="Second slide"
            width="50%"
            height="50%"
          />
          <Carousel.Caption>
            <h5>Second slide label</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <Image
            className="d-block w-100 h-75"
            src="https://wallpapercave.com/wp/wp2928790.jpg"
            alt="Third slide"
            width="50%"
            height="50%"
          />
          <Carousel.Caption>
            <h5>Third slide label</h5>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
