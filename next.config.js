module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["static.vecteezy.com", "wallpapercave.com"],
  },
};
