// import styles from "../styles/Home.module.css";
import Footer from "../components/Footer/Footer";
import Navigation from "../components/Navigation/Navigation";
import { connect } from "react-redux";
import { setInfo } from "../redux/actions/main";
import GameJumbotron from "../components/GameJumbotron/GameJumbotron";
import GameCard from "../components/GameCard/GameCard";
import Image from "next/image";
import { scrollDown, bgImage } from "../public/image/index";
import { Component } from "react";

class Home extends Component {
  state = {
    games: [],
  };
  componentDidMount() {
    fetch(`http://localhost:5000/api/v1/games/`)
      .then((response) => response.json())
      .then((result) => {
        this.setState({ games: result.data });
      });
  }
  render() {
    const { id } = this.props;
    console.log(id);

    return (
      <>
        <Navigation />
        <div
          className="p-5"
          style={{
            backgroundImage: `url(${bgImage})`,
            backgroundSize: "cover",
            height: "270vh",
            width: "100%",
            backgroundAttachment: "fixed",
          }}
        >
          <GameJumbotron id="one" />
          <footer className="d-flex flex-column align-items-center mb-5">
            <p className="text-uppercase m-0 text-white">Choose Game</p>
            <a className="icon-scrolldown" href="#two">
              <Image src={scrollDown} alt="scrolldown" />
            </a>
          </footer>
          <div className="row row mx-auto" id="two">
            {this.state.games.map((data) => {
              return (
                <GameCard
                  key={data.uuid}
                  img={data.image ? data.image : gameImage}
                  title={data.name}
                  uuid={data.uuid}
                  detail={data.detail}
                />
              );
            })}
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return { id: state.main };
};

const mapDispatchToProps = {
  setInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);