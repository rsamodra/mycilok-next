// import React from "react";
import Footer from "../../components/Footer/Footer";
import GameCard from "../../components/GameCard/GameCard";
import Navigation from "../../components/Navigation/Navigation";

export default function Game(props) {
  const { gameData } = props;
  console.log(gameData);
  return (
    <div>
      <Navigation />
      <div className="row row mx-auto p-5">
        {gameData.data.map((data) => {
          return (
            <GameCard
              key={data.uuid}
              img={data.image}
              title={data.name}
              uuid={data.uuid}
              detail={data.detail}
            />
          );
        })}
      </div>
      <Footer />
    </div>
  );
}

// export async function getStaticProps() {
export async function getServerSideProps() {
  const game = await fetch(
    "https://mycilok-backend.herokuapp.com/api/v1/games"
  );
  const gameData = await game.json();
  return {
    props: {
      gameData,
    },
  };
}
