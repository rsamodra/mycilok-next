// import { useRouter } from "next/router";
// import React, { Component } from "react";
import Navigation from "../../components/Navigation/Navigation";
import Link from "next/link";
import Image from "next/image";

export default function GameDetail(props) {
  // const router = useRouter();
  const { game } = props;
  console.log("props", props);
  const gameLink =
    game.data.name === "RPS" ? `/game/play/${game.data.uuid}` : "/";
  const gameClass =
    gameLink === "/"
      ? "btn btn-warning shadow-lg disabled"
      : "btn btn-warning shadow-lg";
  return (
    <div>
      <Navigation />
      <div className="shadow-lg container-fluid bg-secondary rounded-3 mt-5">
        <div className="d-flex align-items-baseline">
          <div className="w-25 text-center text-white my-3 mx-2">
            <Image
              src={game.data.image ? game.data.image : gameImage}
              width={380}
              height={250}
              // layout="fill"
              alt="game"
            />
            <h1>{game.data.name}</h1>
            <p>{game.data.detail}</p>
            <Link href={gameLink}>
              <a className={gameClass}>Play Now!</a>
            </Link>
          </div>
          <div className="w-75 text-center my-3 mx-2">
            <h1>Leaderboard</h1>
            <div className="shadow-sm rounded-3 bg-white h-100">
              {game.data.name} Leaderboard
              <table className="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Rank</th>
                    <th scope="col">Player</th>
                    <th scope="col">Score</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                {game.data.name === "RPS" && (
                  <tbody>
                    {game.data?.leaderboards?.map((item, index) => (
                      <tr key={index}>
                        <th scope="row">{(index += 1)}</th>
                        <td>{item?.user_profile?.full_name}</td>
                        <td>{item.score}</td>
                        <td>
                          <Link
                            href={`/profile/${item?.user_profile?.user_id}`}
                          >
                            <a className="btn btn-info">Peek</a>
                          </Link>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                )}
                {game.data.name !== "RPS" && (
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Mark</td>
                      <td>55</td>
                      <td>
                        <Link href="#">
                          <a className="btn btn-info disabled">Peek</a>
                        </Link>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Jacob</td>
                      <td>43</td>
                      <td>
                        <Link href="#">
                          <a className="btn btn-info disabled">Peek</a>
                        </Link>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>Larry the Bird</td>
                      <td>32</td>
                      <td>
                        <Link href="#">
                          <a className="btn btn-info disabled">Peek</a>
                        </Link>
                      </td>
                    </tr>
                  </tbody>
                )}
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getStaticPaths() {
  const res = await fetch(`https://mycilok-backend.herokuapp.com/api/v1/games`);
  const gameData = await res.json();
  const paths = gameData.data.map((game) => ({
    params: {
      uuid: game.uuid,
    },
  }));
  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps(context) {
  // export async function getServerSideProps(context) {
  const { uuid } = context.params;
  const res = await fetch(
    `https://mycilok-backend.herokuapp.com/api/v1/games/${uuid}`
  );
  const game = await res.json();
  console.log("game", game);

  return {
    props: {
      game,
    },
  };
}
