import React, { Component, useState } from "react";
import Link from "next/link";
// import Router from "next/router";
import firebase from "../../components/services/firebase";
import styles from "./login.module.css";
import { useRouter } from "next/router";

// const user = (props) => {
//   const { dataUsers } = props;
//   const router = useRouter();
// };

const Login = () => {
  // state = {
  //   email: "",
  //   password: "",
  // };
  // const [email, setEmail] = useState("")
  // const [password, setPassword] = useState("")
  const [field, setField] = useState({ email: "", password: "" });
  const router = useRouter();

  const handleChangeField = (e) => {
    // this.setState({ [e.target.name]: e.target.value });
    setField({ ...field, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = field;

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        if (res.user.emailVerified) {
          // console.log("masukRes", res.user.emailVerified);
          console.log("this.props", this.props);
          // this.props.history.push("/");
          router.push(`/`);

          // return {
          //   redirect: {
          //     destination: "/",
          //     permanent: false,
          //   },
          // };
        } else {
          alert("Please Verify your email first...!");
          firebase.auth().signOut();
        }
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  // const { email, password } = field;
  return (
    <div>
      {/* <Navigation /> */}
      <div className={styles.loginBody}>
        <div className={styles.loginContainer}>
          <div className={styles.loginHeader}>
            <h1>Login</h1>
          </div>
          <form onSubmit={handleSubmit}>
            <div id='form' className='form'>
              <div className='my-2 mx-5'>
                <label htmlFor='email' className='form-label'>
                  Email
                </label>
                <input
                  id='email'
                  type='email'
                  name='email'
                  className='form-control mb-4'
                  placeholder='Email'
                  value={field.email}
                  onChange={(e) => handleChangeField(e)}
                />
              </div>
              <div className='my-2 mx-5'>
                <label
                  htmlFor='password'
                  className='form-label font-weight-bold'
                >
                  Password
                </label>
                <input
                  id='password'
                  type='password'
                  name='password'
                  className='form-control mb-4'
                  placeholder='Password'
                  value={field.password}
                  onChange={(e) => handleChangeField(e)}
                />
              </div>
              <div className='text-center'>
                <p>
                  Don`&apos;`t have an account?{" "}
                  <Link href='/register'>Sign up</Link>{" "}
                </p>
              </div>
              <div className='text-center'>
                <p>
                  Back to <Link href='/'>Home</Link>{" "}
                </p>
              </div>
              <button
                type='submit'
                className={styles.formInputBtn}
                onClick={this.showHandler}
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
