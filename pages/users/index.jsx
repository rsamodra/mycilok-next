import Header from "../../components/Header";
import Footer from "../../components/Footer/Footer";
import Navigation from "../../components/Navigation/Navigation";

export default function Users() {
  return (
    <div>
      {/* <Header /> */}
      <Navigation />
      <p className="title">Users Page</p>
      <Footer />
    </div>
  );
}
