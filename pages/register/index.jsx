import React, { Component } from "react";
import Link from "next/link";
import styles from "./register.module.css";
// import firebase from "../../services/firebase";
// import axios from "axios";
import { bgImage } from "../../public/image/index";

class Register extends Component {
  state = {
    username: "",
    email: "",
    date: "",
    password: "",
    loading: false,
  };
  handleChangeField = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ loading: true });
    const { email, password } = this.state;
    let user_id = "";
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        console.log(res);
        user_id = res.user.uid;
        console.log(user_id);
        const user = {
          user_id,
          full_name: this.state.username,
          email: this.state.email,
          birthday: this.state.date,
        };
        console.log(user);
        axios
          .post(`http://localhost:5000/api/v1/user-profile/${user_id}`, user)
          .then((res) => {
            console.log(res);
            console.log(res.data);
            if (res.status) {
              this.props.history.push("/login");
            }
          })
          .catch((error) => {
            alert(error.message);
          });
        firebase
          .auth()
          .currentUser.sendEmailVerification()
          .then((data) => {
            console.log(data);
            alert("Mohon verifikasi email anda");
            this.props.history.push("/login");
          })
          .catch((error) => {
            alert(error.message);
          });
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  render() {
    const { loading } = this.state;
    const { email, password, username, date } = this.state;
    return (
      <div
        className={styles.registerBody}
        style={{
          backgroundImage: `url(${bgImage})`,
          backgroundSize: "cover",
          height: "100%",
          width: "100%",
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
        }}
      >
        <div className={styles.registerContainer}>
          <div className="register-header">
            <h1>Create An Account</h1>
          </div>
          <form onSubmit={this.handleSubmit}>
            <div id="form" className={styles.form}>
              <div className={styles.formInputs}>
                <label htmlFor="username" className={styles.formLabel}>
                  Username
                </label>
                <input
                  id="username"
                  type="text"
                  name="username"
                  className={styles.formInput}
                  placeholder="Username"
                  value={username}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className={styles.formInputs}>
                <label htmlFor="email" className={styles.formLabel}>
                  Email
                </label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  className={styles.formInput}
                  placeholder="Email"
                  value={email}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className={styles.formInput}>
                <label htmlFor="date" className={styles.formLabel}>
                  Date Of Birth
                </label>
                <input
                  id="date"
                  type="date"
                  name="date"
                  className={styles.formInput}
                  placeholder="date"
                  value={date}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className={styles.formInput}>
                <label htmlFor="password" className={styles.formLabel}>
                  Password
                </label>
                <input
                  id="password"
                  type="password"
                  name="password"
                  className={styles.formInput}
                  placeholder="Password"
                  value={password}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className="text-center">
                <p>
                  Already Have An Account?<Link href="/login">Login</Link>{" "}
                </p>
              </div>
              <div className="text-center">
                <p>
                  Back to <Link href="/">Home</Link>{" "}
                </p>
              </div>
              <button
                type="submit"
                className="form-input-btn shadow-lg"
                onClick={this.showHandler}
                disabled={loading}
              >
                {loading && <i className="spinner-border" role="status"></i>}
                {loading && <span> Processing...</span>}
                {!loading && <span>Sign Up</span>}
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Register;
