import styles from "./profile.module.css";
export default function profile() {
  return (
    <div className={styles.profilebody}>
      <div className={styles.wrapper}>
        <div className={styles.left}>
          {/* <img src={profileImage} alt="user" width="200" /> */}
          <h4>username</h4>
          <p>Player Profile</p>
        </div>
        <form>
          <div className={styles.right}>
            <div className={styles.right}>
              <div className={styles.info}>
                <h3>Information</h3>
                <div className={styles.info_data}>
                  <div className={styles.info_data}>
                    <div className={styles.data}>
                      <h4>Username</h4>
                      <input
                        id="username"
                        type="text"
                        name="fullname"
                        className=""
                        placeholder="username"
                        // defaultValue={fullname}
                        // onChange={this.changeHandler}
                        // disabled={!isUpdate}
                      />
                    </div>
                    <div className="data">
                      <h4>Email</h4>
                      <input
                        id="email"
                        type="email"
                        name="email"
                        className=""
                        placeholder="email"
                        // defaultValue={email}
                        // onChange={this.changeHandler}
                        disabled
                      />
                    </div>
                    <div className="data">
                      <h4>Date of Birth</h4>
                      <input
                        id="date"
                        type="date"
                        name="date"
                        className=""
                        placeholder=""
                        // defaultValue={date}
                        // onChange={this.changeHandler}
                        // disabled={!isUpdate}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.mainbutton}>
              <div className={styles.onebutton}>
                <button type="submit" className={styles.profilebutton}>
                  Create
                </button>
              </div>
              <div className={styles.secondbutton}>
                <button type="submit" className={styles.profilebutton}>
                  Edit
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
