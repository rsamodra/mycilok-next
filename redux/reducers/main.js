import { SET_NAME } from "../types";

const main = (
  state = {
    id: 1,
    name: "Ilham",
    phoneNumber: "081949565427",
  },
  action
) => {
  switch (action.type) {
    case SET_NAME:
      return [...state, action.payload];
    default:
      return { ...state };
  }
};

export default main;
